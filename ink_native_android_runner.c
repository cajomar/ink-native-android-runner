#include <android/input.h>
#include <android/keycodes.h>
#include <android/log.h>
#include <android_native_app_glue.h>
#include <jni.h>
#include <string.h>
#include <time.h>

#include <EGL/egl.h>
#include <GLES2/gl2.h>
#include <ink_runner.h>

#define LOG_TAG "ink"
#define LOGV(...) ((void)__android_log_print(ANDROID_LOG_VERBOSE, LOG_TAG, __VA_ARGS__))
#define LOGD(...) ((void)__android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__))
#define LOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__))
#define LOGW(...) ((void)__android_log_print(ANDROID_LOG_WARN, LOG_TAG, __VA_ARGS__))
#define LOGE(...) ((void)__android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__))
#define LOGF(...) ((void)__android_log_print(ANDROID_LOG_FATAL, LOG_TAG, __VA_ARGS__))


void android_fopen_set_asset_manager(AAssetManager* manager);
int main();


struct engine {
    struct android_app* app;
    EGLDisplay display;
    EGLSurface surface;
    EGLContext context;
    struct ink* ink;
    ink_runner_cb post_init;
    ink_runner_cb pre_step;
    ink_runner_post_step post_step;
    ink_runner_unhandled_event unhandled_event;
    bool continuous_step;
    bool running;
    double last_frame;
};


static struct engine* get_engine() {
    static struct engine engine;
    return &engine;
}

static int setup_context(struct engine* engine) {
    LOGD("Creating context\n");
    const EGLint attribs[] = {
        EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
        EGL_BLUE_SIZE, 8,
        EGL_GREEN_SIZE, 8,
        EGL_RED_SIZE, 8,
        EGL_NONE
    };

    EGLDisplay display = eglGetDisplay(EGL_DEFAULT_DISPLAY);

    if (eglInitialize(display, NULL, NULL) == EGL_FALSE) {
        LOGE("Failed to initialize EGLL\n");
        return -1;
    }

    const EGLint context_attributes[] = {
        EGL_CONTEXT_CLIENT_VERSION, 2,
        EGL_NONE
    };

    /* Here, the application chooses the configuration it desires.
     * find the best match if possible, otherwise use the very first one
     */
    EGLint num_configs;
    eglChooseConfig(display, attribs, NULL, 0, &num_configs);
    EGLConfig supportedConfigs[num_configs];
    eglChooseConfig(display, attribs, supportedConfigs, num_configs, &num_configs);
    EGLConfig config = NULL;
    int i = 0;
    for (; i < num_configs; i++) {
        EGLConfig cfg = supportedConfigs[i];
        EGLint r, g, b, d;
        if (eglGetConfigAttrib(display, cfg, EGL_RED_SIZE, &r)   &&
                eglGetConfigAttrib(display, cfg, EGL_GREEN_SIZE, &g) &&
                eglGetConfigAttrib(display, cfg, EGL_BLUE_SIZE, &b)  &&
                eglGetConfigAttrib(display, cfg, EGL_DEPTH_SIZE, &d) &&
                r == 8 && g == 8 && b == 8 && d == 0 ) {

            config = supportedConfigs[i];
            break;
        }
    }
    if (i == num_configs) {
        LOGE("No good config\n");
        config = supportedConfigs[0];
    }

    if (config == NULL) {
        LOGW("Unable to initialize EGLConfig");
        return -1;
    }

    /* EGL_NATIVE_VISUAL_ID is an attribute of the EGLConfig that is
     * guaranteed to be accepted by ANativeWindow_setBuffersGeometry().
     * As soon as we picked a EGLConfig, we can safely reconfigure the
     * ANativeWindow buffers to match, using EGL_NATIVE_VISUAL_ID. */
    EGLint format;
    eglGetConfigAttrib(display, config, EGL_NATIVE_VISUAL_ID, &format);
    EGLSurface surface = eglCreateWindowSurface(display, config, engine->app->window, NULL);
    EGLContext context = eglCreateContext(display, config, NULL, context_attributes);

    if (eglMakeCurrent(display, surface, surface, context) == EGL_FALSE) {
        LOGW("Unable to eglMakeCurrent");
        return -1;
    }

    engine->display = display;
    engine->context = context;
    engine->surface = surface;

    LOGI("OpenGL vendor: %s\n"
         "OpenGL renderer: %s\n"
         "OpenGL version: %s\n"
         "OpenGL extensions: %s\n",
         glGetString(GL_VENDOR),
         glGetString(GL_RENDERER),
         glGetString(GL_VERSION),
         glGetString(GL_EXTENSIONS));

    if (engine->ink) {
        LOGD("Reloading ink renderer\n");
        ink_reload_renderer(engine->ink, NULL);
    }

    // Initialize GL state.
    // glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_FASTEST);
    // glEnable(GL_CULL_FACE);
    // glShadeModel(GL_SMOOTH);
    // glDisable(GL_DEPTH_TEST);
    return 0;
}

void destroy_context(struct engine* e) {
    if (e->display != EGL_NO_DISPLAY) {
        eglMakeCurrent(e->display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
        if (e->context != EGL_NO_CONTEXT) {
            eglDestroyContext(e->display, e->context);
        }
        if (e->surface != EGL_NO_SURFACE) {
            eglDestroySurface(e->display, e->surface);
        }
        eglTerminate(e->display);
    }
    e->display = EGL_NO_DISPLAY;
    e->context = EGL_NO_CONTEXT;
    e->surface = EGL_NO_SURFACE;
}

void engine_handle_cmd(struct android_app* app, int32_t cmd) {
    LOGD("CMD\n");
    struct engine* engine = app->userData;
    switch (cmd) {
        case APP_CMD_INIT_WINDOW:
            LOGD("He want's me to make a window\n");
            if (engine->app->window != NULL) {
                LOGD("Gonna make it\n");
                setup_context(engine);
            }
            break;
        case APP_CMD_TERM_WINDOW:
            destroy_context(engine);
            break;
    }
}

static void display_keyboard(struct engine* engine, bool pShow) {
    // Based on https://stackoverflow.com/questions/5864790/how-to-show-the-soft-keyboard-on-native-activity
    jint lFlags = 0;
    const struct JNINativeInterface * env = 0;
    const struct JNINativeInterface ** envptr = &env;
    const struct JNIInvokeInterface ** jniiptr = engine->app->activity->vm;
    const struct JNIInvokeInterface * jnii = *jniiptr;

    jnii->AttachCurrentThread( jniiptr, &envptr, NULL);
    env = (*envptr);
    jclass activityClass = env->FindClass( envptr, "android/app/NativeActivity");

    // Retrieves NativeActivity.
    jobject lNativeActivity = engine->app->activity->clazz;


    // Retrieves Context.INPUT_METHOD_SERVICE.
    jclass ClassContext = env->FindClass( envptr, "android/content/Context");
    jfieldID FieldINPUT_METHOD_SERVICE = env->GetStaticFieldID( envptr, ClassContext, "INPUT_METHOD_SERVICE", "Ljava/lang/String;" );
    jobject INPUT_METHOD_SERVICE = env->GetStaticObjectField( envptr, ClassContext, FieldINPUT_METHOD_SERVICE );

    // Runs getSystemService(Context.INPUT_METHOD_SERVICE).
    jclass ClassInputMethodManager = env->FindClass( envptr, "android/view/inputmethod/InputMethodManager" );
    jmethodID MethodGetSystemService = env->GetMethodID( envptr, activityClass, "getSystemService", "(Ljava/lang/String;)Ljava/lang/Object;");
    jobject lInputMethodManager = env->CallObjectMethod( envptr, lNativeActivity, MethodGetSystemService, INPUT_METHOD_SERVICE);

    // Runs getWindow().getDecorView().
    jmethodID MethodGetWindow = env->GetMethodID( envptr, activityClass, "getWindow", "()Landroid/view/Window;");
    jobject lWindow = env->CallObjectMethod( envptr, lNativeActivity, MethodGetWindow);
    jclass ClassWindow = env->FindClass( envptr, "android/view/Window");
    jmethodID MethodGetDecorView = env->GetMethodID( envptr, ClassWindow, "getDecorView", "()Landroid/view/View;");
    jobject lDecorView = env->CallObjectMethod( envptr, lWindow, MethodGetDecorView);

    if (pShow) {
        // Runs lInputMethodManager.showSoftInput(...).
        jmethodID MethodShowSoftInput = env->GetMethodID( envptr, ClassInputMethodManager, "showSoftInput", "(Landroid/view/View;I)Z");
        /*jboolean lResult = */env->CallBooleanMethod( envptr, lInputMethodManager, MethodShowSoftInput, lDecorView, lFlags);
    } else {
        // Runs lWindow.getViewToken()
        jclass ClassView = env->FindClass( envptr, "android/view/View");
        jmethodID MethodGetWindowToken = env->GetMethodID( envptr, ClassView, "getWindowToken", "()Landroid/os/IBinder;");
        jobject lBinder = env->CallObjectMethod( envptr, lDecorView, MethodGetWindowToken);

        // lInputMethodManager.hideSoftInput(...).
        jmethodID MethodHideSoftInput = env->GetMethodID( envptr, ClassInputMethodManager, "hideSoftInputFromWindow", "(Landroid/os/IBinder;I)Z");
        /*jboolean lRes = */env->CallBooleanMethod( envptr, lInputMethodManager, MethodHideSoftInput, lBinder, lFlags);
    }

    // Finished with the JVM.
    jnii->DetachCurrentThread( jniiptr );
}

static void
pointer_event(struct engine* engine, enum ink_pointer_action action,
        int x, int y, int pointer_id) {
    struct ink_event ev;
    ev.event_type = INK_EVENT_TYPE_POINTER;
    ev.data.pointer.pointer_id = pointer_id;
    ev.data.pointer.x = x;
    ev.data.pointer.y = y;
    ev.data.pointer.pointer = INK_MOUSE_LEFT;
    ev.data.pointer.action = action;
    if (!ink_event_feed_event(engine->ink, &ev)) {
        if (engine->unhandled_event) engine->unhandled_event(&ev);
    } else {
        if (ink_focus_editable_text_focused(engine->ink)) {
            LOGD("Editable text foused!\n");
            display_keyboard(engine, true);
        }
    }
}

static void
key_event(struct engine* engine, enum ink_key_state state, char c) {
    LOGD("Sending key %c\n", c);
    struct ink_event ev;
    ev.event_type = INK_EVENT_TYPE_TEXT;
    ev.data.text.text = ink_str_from_copy(&c, 1);
    ev.data.text.key_state = state;
    ev.data.text.shift = false;
    ev.data.text.ctrl = false;
    ev.data.text.super = false;
    ev.data.text.alt = false;
    bool r = ink_event_feed_event(engine->ink, &ev);
    if (state == INK_KEY_DOWN || state == INK_KEY_REPEAT) {
        ev.data.text.key_state = INK_TEXT_INPUT;
        r |= ink_event_feed_event(engine->ink, &ev);
    }
    // if (!r && engine->unhandled_event) engine->unhandled_event(&ev);
}

int android_keycode_to_unicode_char(struct engine* engine, int32_t keycode, int32_t meta_state) {
    // https://stackoverflow.com/questions/21124051/receive-complete-android-unicode-input-in-c-c/43871301

    int eventType = AKEY_EVENT_ACTION_DOWN;
    const struct JNINativeInterface * env = 0;
    const struct JNINativeInterface ** envptr = &env;
    const struct JNIInvokeInterface ** jniiptr = engine->app->activity->vm;
    const struct JNIInvokeInterface * jnii = *jniiptr;

    jnii->AttachCurrentThread( jniiptr, &envptr, NULL);
    env = (*envptr);
    //jclass activityClass = env->FindClass( envptr, "android/app/NativeActivity");
    // Retrieves NativeActivity.
    //jobject lNativeActivity = engine->app->activity->clazz;

    jclass class_key_event = env->FindClass( envptr, "android/view/KeyEvent");
    int unicodeKey;

    jmethodID method_get_unicode_char = env->GetMethodID( envptr, class_key_event, "getUnicodeChar", "(I)I");
    jmethodID eventConstructor = env->GetMethodID( envptr, class_key_event, "<init>", "(II)V");
    jobject eventObj = env->NewObject( envptr, class_key_event, eventConstructor, eventType, keycode);

    unicodeKey = env->CallIntMethod( envptr, eventObj, method_get_unicode_char, meta_state );

    // Finished with the JVM.
    jnii->DetachCurrentThread( jniiptr );

    //printf("Unicode key is: %d", unicodeKey);
    return unicodeKey;
}


int32_t engine_handle_input_event(struct android_app* app, AInputEvent* event) {
    struct engine* engine = app->userData;
    switch (AInputEvent_getType(event)) {
        case AINPUT_EVENT_TYPE_MOTION:
            {
                const int pointer_count = AMotionEvent_getPointerCount(event);
                const int32_t action = AMotionEvent_getAction(event);
                int i = (action & AMOTION_EVENT_ACTION_POINTER_INDEX_MASK) >> AMOTION_EVENT_ACTION_POINTER_INDEX_SHIFT;
                int x = AMotionEvent_getX(event, 0);
                int y = AMotionEvent_getY(event, 0);

                switch (action) {
                    case AMOTION_EVENT_ACTION_MOVE:
                        for (i=0; i<pointer_count; i++) {
                            pointer_event(engine, INK_POINTER_MOTION, x, y, i);
                        }
                        break;

                    case AMOTION_EVENT_ACTION_CANCEL:
                        for (i=0; i<pointer_count; i++) {
                            pointer_event(engine, INK_POINTER_UP, x, y, i);
                        }
                        break;

                    case AMOTION_EVENT_ACTION_UP:
                        i = 0;
                    case AMOTION_EVENT_ACTION_POINTER_UP:
                        pointer_event(engine, INK_POINTER_UP, x, y, i);
                        break;

                    case AMOTION_EVENT_ACTION_DOWN:
                        i = 0;
                    case AMOTION_EVENT_ACTION_POINTER_DOWN:
                        pointer_event(engine, INK_POINTER_DOWN, x, y, i);
                        break;

                    default:
                        break;
                }
            }
            break;
        case AINPUT_EVENT_TYPE_KEY:
            {
                // int c = 127 && android_keycode_to_unicode_char(engine, AKeyEvent_getKeyCode(event), AKeyEvent_getMetaState(event));
                char c = 'a';
                if (AKeyEvent_getAction(event) == AKEY_EVENT_ACTION_DOWN) {
                    if (AKeyEvent_getRepeatCount(event) == 0) {
                        key_event(engine, INK_KEY_DOWN, c);
                    } else {
                        key_event(engine, INK_KEY_REPEAT, c);
                    }
                } else {
                    key_event(engine, INK_KEY_UP, c);
                }
            }
            break;
        case AINPUT_EVENT_TYPE_FOCUS:
            break;
    }
    return 0;
}



void android_main(struct android_app* app) {
    LOGD("called android_main\n");
    struct engine* e = get_engine();
    memset(e, 0, sizeof(struct engine));
    app->userData = e;
    app->onAppCmd = engine_handle_cmd;
    app->onInputEvent = engine_handle_input_event;
    e->app = app;
    android_fopen_set_asset_manager(app->activity->assetManager);
    main(0, NULL);
}

INK_EXPORTED
void ink_runner_exit() {
    get_engine()->running = false;
}


void do_input(struct engine* e) {
    int ident;
    int events;
    struct android_poll_source* source;

    while ((ident=ALooper_pollAll(0, NULL, &events, (void**)&source)) >= 0) {

        // Process this event.
        if (source != NULL) {
            source->process(e->app, source);
        }

        // If a sensor has data, process it now.
        if (ident == LOOPER_ID_USER) {
        }

        // Check if we are exiting.
        if (e->app->destroyRequested != 0) {
            destroy_context(e);
        }
    }
}

static void step(struct engine* e) {
    if (e->display == EGL_NO_DISPLAY) return;
    glClearColor(0.f, 0.f, 0.0f, 1.f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    if (e->pre_step) {
        e->pre_step();
    }

    struct timespec spec;
    clock_gettime(CLOCK_REALTIME, &spec);
    double now = spec.tv_sec + spec.tv_nsec/(double)(1000 * 1000 * 1000);
    double dt = now - e->last_frame;
    e->last_frame = now;

    ink_step(e->ink, dt);

    if (e->post_step) {
        e->post_step(dt);
    }

    int32_t w, h;
    eglQuerySurface(e->display, e->surface, EGL_WIDTH, &w);
    eglQuerySurface(e->display, e->surface, EGL_HEIGHT, &h);
    ink_setViewport(e->ink, w, h);

    ink_render(e->ink);
    eglSwapBuffers(e->display, e->surface);
}

INK_EXPORTED
void ink_runner(struct ink_runner_options* options) {
    LOGD("Starting runner\n");
    struct engine* e = get_engine();

    while (!e->display) do_input(e);

    e->post_init = options->post_init;
    e->pre_step = options->pre_step;
    e->post_step = options->post_step;
    e->unhandled_event = options->unhandled_event;
    e->running = true;
    e->ink = ink_get();
    if (!e->ink) e->ink = ink_new(0);

    if (ink_init_renderer(e->ink, INK_RENDERER_API_GL, NULL) != 0) {
        LOGE("ink renderer init failure.\n");
    }
#define DARG(PASSED, DEFAULT) ((PASSED) ? (PASSED) : (DEFAULT))
    ink_load_assets_bin(e->ink, DARG(options->assets_bin_path, "gui_assets.bin"));
    ink_load_gui_bin(e->ink, DARG(options->assets_bin_path, "gui.bin"));
#undef DARG
    ink_instantiate_main_template(e->ink);

    if (e->post_init) {
        LOGD("Calling post_init()\n");
        /* glClearColor(1.f, 1.f, 1.f, 1.f);
           glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT); */
        e->post_init();
    }

    while (e->running) {
        do_input(e);
        step(e);
    }
    ink_delete(e->ink);
    ink_free_static_globals();
}
