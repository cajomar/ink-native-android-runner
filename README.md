A C/C++ ink runner for android.
Uses [android-fopen](https://bitbucket.org/cajomar/android-fopen).  
Add this as a subdirectory along with ink after android-fopen:
```cmake
add_subdirectory(../android-fopen android-fopen)
set(UIINK_BUILD_RUNNER OFF CACHE BOOL "")
add_subdirectory(../ink ink)
add_subdirectory(../ink-native-android-runner ink_runer)
```
Touch events work. For some reason text events cause a crash.